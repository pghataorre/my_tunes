function mainController($http, $rootScope) {

    const vm = this;
    const err = 'SORRY CANT BE LOADED';
    let trackResults = {};
    let favTrackArr = [];

    function getApiResults(filterVal) {

        const urlPath = 'https://itunes.apple.com/search?term=' + filterVal + '&limit=25';

        $http({
                method: 'GET',
                url: urlPath
            })
          .then((response) => {

              trackResults = response.data;


          })
          .catch((err) => {

              vm.error = err;

          });

    }

    function searchInfo(searchTerm) {

        let trackInfo = searchTerm;

        if (trackInfo.length < 3) {
            return false;
        }

        trackInfo = trackInfo.replace(/[ ]/g, '+');
        getApiResults(trackInfo);

        return trackInfo;
    }

    function removeFromFavs(trackId, indexVal){

        vm.favs.splice(indexVal, 1);

        if (vm.favs.length < 1){
            vm.hasFavs = false;
        }

    }

    function addToFavsArr(trackId, indexVal) {

        if (!checkFavs(trackId,indexVal)) {
            setFavs(indexVal);
        }

    }

    function checkFavs(trackId, indexVal, favsObj) {

        let i = 0;
        let inArray = false;
        let favCollection = (favsObj) ? favsObj : vm.favs;

        for (i; i < favCollection.length; i++) {

            if (favCollection[i].trackId === trackId) {

                inArray = true;
                alertDuplicateEntryAttempt();
                return true;

            }

        }

        return inArray;

    }

    function setFavs(indexVal){

        favTrackArr.push(trackResults.results[indexVal]);
        vm.hasFavs = true;

    }

    function getFavs(){

        return favTrackArr;

    }

    function getTrackResultsCount() {

        return (getResults().resultCount) ? getResults().resultCount : 'No Results';

    }

    function alertDuplicateEntryAttempt() {

        vm.hasDuplicateFav = true;

        setTimeout(()=> {

            $rootScope.$apply(()=> {

                vm.hasDuplicateFav = false;

            });

        }, 3000);

    }

    function getResults () {

        return trackResults;

    }

    vm.getTrackResultsCount = getTrackResultsCount;
    vm.getResults = getResults;
    vm.addToFavsArr = addToFavsArr;
    vm.searchInfo = searchInfo;
    vm.removeFromFavs = removeFromFavs;
    vm.setFavs = setFavs;
    vm.checkFavs = checkFavs;
    vm.hasFavs = false;
    vm.hasDuplicateFav = false;
    vm.favs = getFavs();

}

export default mainController;
