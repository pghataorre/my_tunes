"use strict";

import app from '../app.js';
import angular from 'angular';
import 'angular-mocks/ngMock';
import favArr from '../testdata/favArr.json';

describe('MY TUNES TEST', ()=>{

    var ctrl,
        vm = {},
        scope = {};

    beforeEach(() => {

        angular.mock.module('app');

    });

    beforeEach(() =>{

        angular.mock.inject((_$controller_, $rootScope)=> {

            vm = $rootScope.$new();
            ctrl = _$controller_('main', { $scope: vm });

        })
    });

    it('Should return FALSE with less than 3 characters', ()=> {

        expect(ctrl.searchInfo('Ma')).toBe(false);

    });

    it('Should return TRACK INFO with + for spaces', ()=> {

        expect(ctrl.searchInfo('Test Artist Name')).toEqual('Test+Artist+Name');

    });

    it('Should Test if a track is ALREADY ADDED in Favourites', ()=> {

        expect(ctrl.checkFavs(689006755, 1, favArr)).toBe(true);

    });

    it('Should Test if a track is NOT in Favourites', ()=> {

        expect(ctrl.checkFavs(123456789, 1, favArr)).toBe(false);

    });

    it('Should show NO RECORDS when there are NO tracks Searched', ()=> {

        expect(ctrl.getTrackResultsCount()).toEqual('No Results');

    });

});