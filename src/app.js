import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import ngRoute from 'angular-route'
import routeConfig from './app.route';
import mainController from './components/mainController'
import './scss/main.scss';

angular.module('app', [ngSanitize, ngRoute])
    .config(routeConfig)
    .controller('main', mainController)
    .name;
