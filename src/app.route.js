function routeConfig($routeProvider) {

  $routeProvider
    .when('/index.html', {
      templateUrl: '../index.html'
    })
    .otherwise({
      redirectTo: '/'
    });

}

export default routeConfig;