var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

  entry: './src/app.js',
  output: {
    filename: 'app.all.js',
    path: path.join(__dirname, "build"),
    publicPath: '/build/'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, "src"),
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.s?(a|c)ss$/,
        include: path.join(__dirname, "src"),
        loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!sass-loader' })
      },
      {
        test: /\.html$/,
        loaders: 'raw-loader'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('styles.css'),
  ],
  devtool: 'source-map',
  watch: true,
  devServer: {
    hot: true
  }
};